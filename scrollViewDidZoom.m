-(void)scrollViewDidZoom:(UIScrollView *)scrollView{
  
 CGFloat t_zs = scrollView.zoomScale;
 t_zs = MAX(t_zs, 0.9f);
 t_zs = MIN(t_zs, 5.0f);
  
 CGFloat x = scrollView.frame.size.width/2;
 CGFloat y = scrollView.frame.size.height/2;
  
 x = scrollView.contentSize.width > scrollView.frame.size.width? scrollView.contentSize.width/2: x;
 y = scrollView.contentSize.height > scrollView.frame.size.height? scrollView.contentSize.height/2: y;
  
 [m_imageView setCenter:CGPointMake(x, y)];
  
}